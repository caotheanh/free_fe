import {configureStore} from "@reduxjs/toolkit";
import createSagaMiddleware from 'redux-saga';
import rootReducer from "../reducers/index";
import rootSaga from "../saga/index";

const sagaMiddleware = createSagaMiddleware()
const store = configureStore({
  reducer:rootReducer,
  middleware:[
    sagaMiddleware
  ]
});

export default store;
sagaMiddleware.run(rootSaga)