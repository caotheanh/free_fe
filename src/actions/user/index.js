import { createAction } from "@reduxjs/toolkit";

export const login = createAction("User/login");
export const loginSuccess = createAction("User/login-success");
export const loginFailed = createAction("User/login-failed");