import { createAction } from "@reduxjs/toolkit";

export const getAllInfo = createAction("Info/getAll");
export const getAllInfoSuccess = createAction("Info/get-success");
export const getAllInfoFailed = createAction("Info/get-failed");