import { fork } from "redux-saga/effects";
import inFoSaga from "./Info";
import userSaga from "./user";

export default function* rootSaga() {
  yield fork(inFoSaga)
  yield fork(userSaga)
}