import axios from "axios";
import { api } from "../../config/api";
import *as userAction from "../../actions/user";
import { put, takeLatest } from "redux-saga/effects";
function* login() {
  try {
    const data = yield axios.get(`${api}/user/login`);
    yield put(userAction.loginSuccess(data.data))
  } catch (error) {
    yield put(userAction.loginFailed(error));
  }
}

export default function* userSaga() {
  yield takeLatest(userAction.login, login)
}