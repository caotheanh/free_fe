import axios from "../../config/axios";
import { api } from "../../config/api";
import *as infoAction from "../../actions/Info";
import { put, takeLatest } from "redux-saga/effects";
function* getInfo() {
  try {
    const data = yield axios.get(`${api}/`);
    yield put(infoAction.getAllInfoSuccess(data.data))
  } catch (error) {
    yield put(infoAction.getAllInfoFailed(error));
  }
}

export default function* getInfoSaga () {
  yield takeLatest(infoAction.getAllInfo, getInfo)
}