import React, { useState } from 'react';
import 'antd/dist/antd.css';

import { Route, Switch, Redirect } from "react-router-dom";
import Login from './component/login/Login';
import Layouts from "./component/Layout";

function App() {
  const [collapsed, setCollapsed] = useState(false);
  const toggle = () => {
    setCollapsed(!collapsed)
  }

  return (
    <Switch>
      <Route exact path="/login">
        <Login />
      </Route>
      <PrivateRoute path="/">
        <Layouts toggle={toggle} collapsed={collapsed} />
      </PrivateRoute>
    </Switch>
  );
}
function PrivateRoute({ children, ...rest }) {
  const data = localStorage.getItem("auth");
  return (
    <Route
      {...rest}
      render={({ location }) =>
      data ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location }
            }}
          />
        )
      }
    />
  );
}

export default App;
