import axios from "axios";

const token = localStorage.getItem("auth");
axios.defaults.headers.common = { 'x-access-token': token }
export default axios;
