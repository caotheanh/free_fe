import { combineReducers } from "redux";
import inFoReducer from "./Info";
import userReducer from "./user";

const rootReducer = combineReducers({
  inFoReducer,
  userReducer
})

export default rootReducer;