import { createReducer } from "@reduxjs/toolkit";
import * as userAction from "../../actions/user";

const initialState = {
  user: [],
  error:[]
}

const userReducer = createReducer(initialState, {
  [userAction.loginSuccess]: (state, action) => ({
    ...state,
    user: action.payload
  }),
  [userAction.loginFailed]: (state, action) => ({
    ...state,
    error: action.payload
  }),
});
export default  userReducer;