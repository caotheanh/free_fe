import { createReducer } from "@reduxjs/toolkit";
import * as inFoActions from "../../actions/Info";

const initialState = {
  info: [],
  error:[]
}

const inFoReducer = createReducer(initialState, {
  [inFoActions.getAllInfoSuccess]: (state, action) => ({
    ...state,
    info: action.payload
  }),
  [inFoActions.getAllInfoFailed]: (state, action) => ({
    ...state,
    error: action.payload
  }),
});
export default  inFoReducer;