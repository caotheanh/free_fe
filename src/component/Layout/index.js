import React, { useState, useEffect } from 'react';
import { Layout } from 'antd';
import LeftMenu from "../LeftMenu/Menu";
import Content from "../Content/index";
import { api } from "../../config/api";
import axios from "../../config/axios";
import { useHistory } from 'react-router';

function Layouts(props) {
  const { toggle, collapsed } = props;
  const history = useHistory();
  const [phone, setPhone] = useState(null);

  useEffect(() => {
    getUserByToken()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const getUserByToken = async () => {

    try {
      const token = localStorage.getItem("auth");
      const user = await axios({
        baseURL: `${api}/users/verify-token`,
        method: "POST",
        data: {
          token
        }
      })
      const now = new Date().getTime();
      const expiredAt = new Date(`${user.data.data.expiredAt}`).getTime();
      if (+now < +expiredAt) {
        setPhone(user.data.data.user)
      } else {
        localStorage.removeItem("auth")
        history.push("/login");
      }
    } catch (error) {
      history.push("/login");
    }

  }
  return (
    <Layout>
      <LeftMenu phone={phone} collapsed={collapsed} />
      <Content toggle={toggle} collapsed={collapsed} />
    </Layout>
  );
}

export default Layouts;