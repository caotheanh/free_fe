import React, { useEffect } from 'react';
import { Form, Input, Button, notification } from 'antd';
import axios from "axios";
import { api } from "../../config/api";
import { useHistory } from 'react-router';

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

const Login = () => {
  useEffect(() =>{
    localStorage.removeItem("auth")
  })
  const history = useHistory();
  const onFinish = async (values) => {
    const data = await axios.post(`${api}/users/login`, values);
    if (data.data && !data.data.data.messages) {
      localStorage.setItem("auth", data.data.data)
      notification.open({
        message: `Login Success`,
      });
      history.push("/")
    } else {
      notification.open({
        message: `${data.data.data.messages}`,

      });
    }

  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Form
      {...layout}
      name="basic"
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}

      style={{
        marginTop:"100px",
        width:"600px",
        margin: "100px auto"

      }}
    >
      <Form.Item
        label="Phone"
        name="phone"
        rules={[
          {
            required: true,
            message: 'Please input your phone!',
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[
          {
            required: true,
            message: 'Please input your password!',
          },
        ]}
      >
        <Input.Password />
      </Form.Item>
      <Form.Item {...tailLayout}>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
};

export default Login;