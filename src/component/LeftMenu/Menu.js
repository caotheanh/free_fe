import React from 'react';
import { Layout, Menu } from 'antd';
import { Link } from 'react-router-dom';


const { Sider } = Layout;

function LeftMenu(props) {
  const { collapsed, phone } = props;

  return (
    <Sider trigger={null} collapsible collapsed={collapsed}>
      <div className="logo">{phone}</div>
      <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
        <Menu.Item key="1">
          <Link to="/">
            Thống kê
         </Link>
        </Menu.Item>
        <Menu.Item key="2">
          <Link to="/list">
            Thông tin
          </Link>
        </Menu.Item>
      </Menu>
    </Sider>
  );
}

export default LeftMenu;