import React from 'react';
import { Layout } from 'antd';
import {
  MenuUnfoldOutlined,
} from '@ant-design/icons';
import Routers from "../routers";

const { Header, Content } = Layout;

function index(props) {
  const { toggle, collapsed } = props;


  return (
    <Layout className="site-layout">
      <Header className="site-layout-background" style={{ padding: 0 }}>
        <div onClick={toggle} style={{ marginLeft: "40px" }}>
          {collapsed ? <MenuUnfoldOutlined /> : <MenuUnfoldOutlined />}
        </div>

      </Header>
      <Content
        className="site-layout-background"
        style={{
          margin: '24px 16px',
          padding: 24,
          minHeight: 280,
        }}
      >
        < Routers />
      </Content>
    </Layout>
  );
}

export default index;