import React from 'react';
import { Switch, Route } from "react-router-dom";
import Chart from '../Chart';
import List from '../List/List';

function index() {
  return (
    <Switch>
      <Route exact path="/">
        <Chart />
      </Route>
      <Route path="/list">
        <List />
      </Route>
    </Switch>
  );
}
export default index;