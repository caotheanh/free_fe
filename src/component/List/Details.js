import React, { useState } from 'react';
import { Modal, Select, notification, Input, Button } from 'antd';
import axios from "../../config/axios";
import { api } from "../../config/api";

const { Option } = Select;
const { TextArea } = Input;

function Details(props) {
  const { isModalVisible, handleOk, dataDetail } = props;
  const [valueSelect, setValueSelect] = useState("");
  const [valueText, setValueText] = useState("")
  const token = localStorage.getItem("auth")
  const onUpdate = async () => {
    const result = await axios({
      baseURL: `${api}/info/update`,
      method: "PUT",
      data: {
        id: dataDetail.id,
        status: valueSelect,
        result_note: valueText,
      },
      headers: {
        token
      }
    })
    result.data.data && setValueText("")
    result.data.data ? notification.open({
      message: `Cập nhật trạng thái thành công`,
    }) : notification.open({
      message: `Cập nhật trạng thái thất bại`
    });;
  }

  const changeStatus = async (values) => {
    setValueSelect(values)

  }

  const onChange = e => {
    setValueText(e.target.value);
  };

  return (
    <Modal title={dataDetail && dataDetail.name} visible={isModalVisible} onOk={handleOk} cancelButtonProps={{ style: { display: 'none' } }}>
      <p>Name: <b>{dataDetail.name}</b></p>
      <p>Date of birth: <b>{dataDetail.dateOfBirth}</b></p>
      <p>Phone: <b>{dataDetail.phone}</b></p>
      <p>Address: <b>{dataDetail.address}</b></p>
      {dataDetail.result_note ? <p>Detail: <b>{dataDetail.result_note}</b></p> : ""}
      <p>Status: <b>{dataDetail.status === 1 ? "Chưa liên hệ" : dataDetail.status === 2
        ? "Chưa liên hệ được" : dataDetail.status === 3
          ? "Báo bận" : "Đồng ý"}</b></p>
      <Select defaultValue="Chọn trạng thái" style={{ width: 240 }} onChange={changeStatus}>
        <Option value="1">Chưa liên hệ</Option>
        <Option value="2">Chưa liên hệ được</Option>
        <Option value="3">Báo bận</Option>
        <Option value="4">Đồng ý</Option>
      </Select>
      <p style={{ marginTop: "20px" }}>Chi tiết</p>
      <TextArea showCount onChange={onChange} />
      <div style={{ marginTop: "20px" }}><Button type="primary" onClick={onUpdate}>Cập nhật</Button></div>
    </Modal>
  );
}

export default Details;