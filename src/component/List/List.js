/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState, memo } from 'react';
import { api } from "../../config/api";
import axios from "../../config/axios";
import { Table, Space, Button, Pagination, Select } from 'antd';
import moment from "moment";

import Details from './Details';

const { Column } = Table;
const { Option } = Select;

function List() {
  const [dataInfo, setData] = useState([])
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [dataDetail, setDataDetail] = useState({});
  const [count, setCount] = useState(0);
  const [page, setPage] = useState(1);

  const SIZE = 10;

  useEffect(() => {
    getData()
  }, [page])


  const showModal = async (id) => {
    const result = await axios({
      baseURL: `${api}/info/detail/${id}`,
      method: "GET",
    })
    result.data.data && setDataDetail(result.data.data)
    setIsModalVisible(true);
  };

  const filByStatus = async (status) => {
    if (+status === 0) {
      getData()
    } else {
      setPage(1);
      const result = await axios({
        baseURL: `${api}/info/status`,
        params: {
          page,
          size: SIZE,
          status
        }
      })
      result.data.data.rows && setData(result.data.data.rows);
      result.data.data.count && setCount(result.data.data.count)
    }


  }

  const handleOk = () => {
    getData()
    setIsModalVisible(false);
  };

  const getData = async () => {
    try {
      const result = await axios({
        method: "GET",
        baseURL: `${api}/info`,
        params: {
          page
        }
      })
      result.data && result.data.data.rows && setData(result.data.data.rows)
      result.data && result.data.data.count && setCount(result.data.data.count)
    } catch (error) {
      console.log(error);
    }
  }


  const changePage = page => {
    setPage(page)
  };


  return (
    <>
      <Details isModalVisible={isModalVisible} handleOk={handleOk} dataDetail={dataDetail ? dataDetail : {}} />
      <Select defaultValue="Tất cả" onChange={filByStatus}
        style={{ float: "right", marginBottom: "30px", marginRight: "50px", width: "400px" }}>
        <Option value="0">Tất cả</Option>
        <Option value="1">Chưa liên hệ</Option>
        <Option value="2">Chưa liên hệ được</Option>
        <Option value="3">Báo bận</Option>
        <Option value="4">Đồng ý</Option>
      </Select>
      <Table dataSource={dataInfo} pagination={false}>
        <Column title="STT" dataIndex="id" key="id" />
        <Column title="Name" dataIndex="name" key="name" />
        <Column title="Date of birth" dataIndex="date_of_birth" key="date_of_birth" />
        <Column title="Phone" dataIndex="phone" key="phone" />
        <Column title="Address" dataIndex="address" key="address" />
        <Column title="Status" dataIndex="status" key="status"
          render={(record) => (
            record === 1 ? "Chưa liên hệ" : record === 2
              ? "Chưa liên hệ được" : record === 3
                ? "Báo bận" : "Đồng ý")} />
        <Column title="Ngày gửi" dataIndex="createdAt" key="createdAt" render={(record) => (moment(record).format('MM/DD/YYYY')
        )} />
        <Column
          title="Action"
          key="action"
          render={(record) => (
            <Space size="middle">
              <Button onClick={() => showModal(record.id)} >Xem</Button>
            </Space>
          )}
        />
      </Table>
      <Pagination current={page} onChange={changePage} total={count} pageSize={SIZE} style={{ margin: "20px 0" }} />
    </>
  );
}

export default memo(List);